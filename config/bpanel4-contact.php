<?php

declare(strict_types=1);

return [
    'blacklisted_words' => [
        'Аlеnа, de Rumanіа',
        'cheap-seo-packages',
        'best cardholder video',
        'и',
        'Attention: Accounts Payable',
        'follar',
        'wordpress',
        'Feedback Forms',
        'speed-seo.net',
        'Google Map Stacking',
        'advertising',
        'Russian',
        'Nude Models',
        'Domain Authority',
        'backlinks',
        'semrush',
        'wilguiti.cf',
        'restauración de plomería',
        'feedback forms',
        'Banks.credit',
        'banks.credit',
        'casino',
        'Free Traffic',
        'risk-free',
        'kinks',
        'anal sex',
        'bondage',
        'spanked',
        'ximera',
        'Ximera',
        'software',
        'domain authority',
        'monkeydigital',
        'ahrefs',
        'casino',
        'business proposal',
        'SEO Experts',
        'Quantum medicine',
        'Buy Healy now',
        'Cialis',
        'cialis',
        'Viagra',
        'viagra',
        'Hæ, ég vildi vita verð þitt',
        'marketplacesrf.ru',
        'მინდოდა',
        'ვიცოდე',
        'თქვენი',
        'eisiau gwybod eich pris',
        'theastaigh',
        'fháil',
    ],
    'blacklisted_domains' => [
        'mgfzsd.com',
        'list.ru',
        'rstudy.com',
        'avio.alc-nnov.ru',
        'lifeyogaworld',
        'ufimskayamebel.ru',
        'yandex.com',
        'megaremont.pro',
        'gmx.com',
        'mcfaddingroup.com',
        'rockconcertnews.com',
        'alc-nnov.ru',
        'wowzilla.ru',
        'agenciasubido.com',
        'occomputerpros.com',
        'mail2.pro',
        'rambler.ru',
        'iunskiygipertonik@gmail.com',
        'yandex.ru',
        'ra.org',
        'registry.godaddy',
    ],
];
