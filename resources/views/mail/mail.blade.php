@extends('bpanel4-public.mail.base-mail')
@section('preheader')@endsection
@section('content')
    <p>Se ha recibido un nuevo mensaje en {{ $appName }}</p>

    <table>
        <tr>
            <th>Nombre</th>
            <td>{{ $data['name'] }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $data['email'] }}</td>
        </tr>
        <tr>
            <th>Teléfono</th>
            <td>{{ $data['phone'] }}</td>
        </tr>
        <tr>
            <th>Mensaje</th>
            <td>{{ $data['message'] }}</td>
        </tr>
    </table>
@endsection
