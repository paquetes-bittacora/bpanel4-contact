@extends('bpanel4-public.layouts.regular-page')
@section('title')
    Contacto
@endsection
@livewireStyles
@section('content')
    <div>
        <h1>Contacto</h1>
        {{--
        Con el siguiente código se pueden mostrar los campos que se configuran desde el panel. No los incluyo en la
        vista de ejemplo.
        {!! $contactDetails->introductory_text !!}
        {!! $contactDetails->contact_details !!}
        <x-bpanel4-map :latitude="$contactDetails->latitude" :longitude="$contactDetails->longitude" :zoom="$contactDetails->zoom" name="contact_map"/>
        --}}
        <div>
            <form class="bpanel4-contact-form" method="post" action="{{ route('send-contact') }}">
                <input name="name" type="text" maxlength="255" placeholder="Nombre y apellidos" class="name">
                <div class="contact-details">
                    <input type="email" name="email" maxlength="255" placeholder="Correo electrónico">
                    <input type="tel" name="phone" placeholder="Teléfono">
                </div>
                <textarea name="message"
                          placeholder="Escriba su duda o consulta de forma breve, le responderemos lo antes posible."></textarea>
                <div class="acceptance">
                    <label><input type="checkbox" name="acceptance" value="1" required> Confirmo que he leído la información básica sobre protección de datos y la política de privacidad y autorizo el registro de mis datos para los fines indicados.</label>
                </div>
                <div class="recaptcha">
                    {!! NoCaptcha::display() !!}
                @if (isset($errors) && $errors->has('g-recaptcha-response'))
                        <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <button type="submit">Enviar</button>
                </div>
                @csrf
            </form>
        </div>
    </div>
@endsection
@livewireScripts
{!! NoCaptcha::renderJs() !!}
