@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Contacto')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-contact::datatable.show') }}</span>
            </h4>
        </div>
        <div class="card-body">
            <table>
                <tr>
                    <th class="pb-2 pr-2">Hora de envío:</th>
                    <td class="pb-2">{{ date_format($submission->created_at, 'd-m-Y H:i:s') }}</td>
                </tr>
                <tr>
                    <th class="pb-2 pr-2">Nombre:</th>
                    <td class="pb-2">{{ $submission->name }}</td>
                </tr>
                <tr>
                    <th class="pb-2 pr-2">Email:</th>
                    <td class="pb-2">{{ $submission->email }}</td>
                </tr>
                <tr>
                    <th class="pb-2 pr-2">Teléfono:</th>
                    <td class="pb-2">{{ $submission->phone }}</td>
                </tr>
                <tr>
                    <th class="pb-2 pr-2" style="vertical-align: top">Mensaje:</th>
                    <td class="pb-2">{!! nl2br(strip_tags($submission->message)) !!}</td>
                </tr>
            </table>
        </div>
    </div>


@endsection
