@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar detalles de contacto')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">Editar detalles de contacto</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('contact-admin.update')}}">
            @method('PUT')
            @csrf
            @if($errors->any())
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            @endif

            @livewire('utils::tinymce-editor', ['name' => 'introductory_text', 'labelText' => __('bpanel4-contact::contact_info.introductory_text'), 'value' => old('introductory_text') ?? $contactInfo->introductory_text ])
            @livewire('utils::tinymce-editor', ['name' => 'contact_details', 'labelText' => __('bpanel4-contact::contact_info.contact_details'), 'value' => old('contact_details') ?? $contactInfo->contact_details ])

            <x-bpanel4-map-input name="location" description="Haga click en el mapa para establecer la ubicación que se mostrará en la página de contacto." :latitude="$contactInfo->latitude" :longitude="$contactInfo->longitude" :zoom="$contactInfo->zoom"/>

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>

@endsection
