<td>
    {{ date_format($row->created_at, 'd-m-Y H:i') }}
</td>
<td>
    {{ $row->name }}
</td>
<td>
    {{ $row->email }}
</td>
<td>
    {{ $row->phone }}
</td>
<td>
    {{ substr(strip_tags($row->message), 0, 120) }} @if (strlen(strip_tags($row->message)) > 120)...@endif
</td>
<td class="text-center">
    <a href="{{ route('contact-admin.show', ['submission' => $row->id]) }}"><i class="fa fa-eye"></i></a>
    <form method="POST" class="d-inline mx-2"
          action="{{route('contact-admin.destroy', ['submission' => $row->id])}}">
        {!!method_field('DELETE')!!}
        @csrf
        <button id="delete_button{{$row->id}}" type="submit" class="btn btn-primary-outline text-danger"
                onclick="return confirm('¿Está seguro de querer borrar este mensaje?')">
            <i class="fa fa-trash"></i>
        </button>
    </form>
</td>
