<?php

declare(strict_types=1);

return [
    'contact' => 'Kontakt',
    'fullname' => 'Pełne imię i nazwisko',
    'email' => 'Email',
    'phone' => 'Telefon',
    'message-placeholder' => 'Napisz krótko swoje pytanie lub zapytanie, a my odpowiemy tak szybko, jak to możliwe',
    'privacy-acceptance' => 'Potwierdzam, że zapoznałem się z podstawowymi informacjami o ochronie danych oraz <a href="/pagina/politica-privacidad" target="_blank">polityką prywatności</a> i wyrażam zgodę na rejestrację moich danych we wskazanych celach .',

];