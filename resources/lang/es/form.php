<?php

declare(strict_types=1);

return [
    'contact' => 'Contacto',
    'fullname' => 'Nombre completo',
    'email' => 'Correo electrónico',
    'phone' => 'Telefono',
    'message-placeholder' => 'Escriba su duda o consulta de forma breve, le responderemos lo antes posible.',
    'privacy-acceptance' => 'Confirmo que he leído la información básica sobre protección de datos y la <a href="/pagina/politica-privacidad" target="_blank">política de privacidad</a> y autorizo el registro de mis datos para los fines indicados.',
];