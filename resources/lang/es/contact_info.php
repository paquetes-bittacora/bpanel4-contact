<?php

declare(strict_types=1);

return [
    'introductory_text' => 'Texto introductorio',
    'contact_details'=> 'Detalles de contacto',
];
