<?php

declare(strict_types=1);

return [
    'contact-admin' => 'Mensajes recibidos',
    'index' => 'Listado',
    'show' => 'Mostrar',
    'edit' => 'Editar detalles de contacto'
];
