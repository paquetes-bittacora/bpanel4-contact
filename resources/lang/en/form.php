<?php

declare(strict_types=1);

return [
    'contact' => 'Contact',
    'fullname' => 'Full Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'message-placeholder' => 'Briefly write your question or query, we will reply as soon as possible',
    'privacy-acceptance' => 'I confirm that I have read the basic information on data protection and the <a href="/pagina/politica-privacidad" target="_blank">privacy policy</a> and consent to the recording of my data for the purposes indicated.',

];