<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Contact\Http\Controllers\ContactAdminController;
use Bittacora\Bpanel4\Contact\Http\Controllers\ContactPublicController;
use Illuminate\Support\Facades\Route;

Route::middleware(['web'])->group(function () {
    Route::get('/contacto', [ContactPublicController::class, 'index'])->name('contact');
    Route::post('/contacto', [ContactPublicController::class, 'send'])->name('send-contact');
});


Route::prefix('bpanel/contacto')->name('contact-admin.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/editar', [ContactAdminController::class, 'edit'])->name('edit');
        Route::put('/editar', [ContactAdminController::class, 'update'])->name('update');
        Route::get('/listar', [ContactAdminController::class, 'index'])->name('index');
        Route::get('/ver/{submission}', [ContactAdminController::class, 'show'])->name('show');
        Route::delete('/eliminar/{submission}', [ContactAdminController::class, 'destroy'])->name('destroy');
    });
