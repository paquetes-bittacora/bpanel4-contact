<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'contact_infos';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->text('introductory_text');
            $table->text('contact_details');
            $table->decimal('latitude', 12,9);
            $table->decimal('longitude', 12, 9);
            $table->unsignedSmallInteger('zoom');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
