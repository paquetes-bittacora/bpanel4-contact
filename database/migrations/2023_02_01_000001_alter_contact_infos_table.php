<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'contact_infos';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->text('introductory_text')->nullable()->change();
            $table->text('contact_details')->nullable()->change();
            $table->decimal('latitude', 12,9)->nullable()->change();
            $table->decimal('longitude', 12, 9)->nullable()->change();
            $table->unsignedSmallInteger('zoom')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->text('introductory_text')->change();
            $table->text('contact_details')->change();
            $table->decimal('latitude', 12, 9)->change();
            $table->decimal('longitude', 12, 9)->change();
            $table->unsignedSmallInteger('zoom')->change();
        });
    }
};
