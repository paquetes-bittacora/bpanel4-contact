# bPanel4 Contact

Paquete para crear fácilmente un formulario de contacto básico.

## Instalación

El paquete se instala automáticamente, pero una vez instalado hay que configurarlo completando los
valores de `NOCAPTCHA_SECRET` y `NOCAPTCHA_SITEKEY` en el archivo `.env`

En la vista `form.blade.php` hay un comentario con el código que mostraría el mapa y el texto introducido en el panel de control. No se muestra por defecto, así que hay que si se quiere mostrar, hay que copiar ese archivo a `resources/views/vendor/bpanel4-contact/form.blade.php` y editarlo.
