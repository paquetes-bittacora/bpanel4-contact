<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Models;

use Illuminate\Database\Eloquent\Model;

final class ContactInfo extends Model
{
    /** @var array */
    public $guarded = [];
}
