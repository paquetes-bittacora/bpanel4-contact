<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property bool $acceptance
 * @method Builder<ContactSubmission> query()
 * @method Builder<ContactSubmission> orderBy()
 * @method Builder<ContactSubmission> when()
 */
final class ContactSubmission extends Model
{
    /** @var array */
    public $guarded = [];
}
