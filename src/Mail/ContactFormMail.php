<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Mail;

use Illuminate\Mail\Mailable;

final class ContactFormMail extends Mailable
{
    public function __construct(private readonly array $data)
    {
    }

    public function build(): ContactFormMail
    {
        return $this->subject('Nuevo mensaje desde el formulario de contacto de ' . config('app.name'))
            ->view('bpanel4-contact::mail.mail', [
                'data' => $this->data,
                'appName' => config('app.name'),
            ]);
    }
}
