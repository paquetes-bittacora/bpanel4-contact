<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact;

use Bittacora\Bpanel4\Contact\Commands\InstallCommand;
use Bittacora\Bpanel4\Contact\Http\Livewire\SubmissionsDatatable;
use Illuminate\Support\ServiceProvider;
use Livewire\LivewireManager;

final class Bpanel4ContactServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-contact';

    public function boot(LivewireManager $livewire): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->commands(InstallCommand::class);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'contact-admin');
        $this->publishes([__DIR__ . '/../config/bpanel4-contact.php' => config_path('bpanel4-contact.php')]);

        $livewire->component(self::PACKAGE_PREFIX. '::livewire.submissions-table', SubmissionsDatatable::class);
    }
}
