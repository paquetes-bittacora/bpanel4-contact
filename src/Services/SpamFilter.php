<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Services;

use Bittacora\Bpanel4\Contact\Exceptions\MessageIsBlacklistedException;
use Illuminate\Contracts\Config\Repository;

final class SpamFilter
{
    public function __construct(private readonly Repository $config)
    {
    }

    public function filter(array $data): void
    {
        if ($this->config->has('bpanel4-contact.blacklisted_domains')) {
            $this->filterByDomain($data);
        }

        if ($this->config->has('bpanel4-contact.blacklisted_words')) {
            $this->filterByMessage($data);
        }
    }

    private function filterByDomain(array $data): void
    {
        $blacklistedDomains = $this->config->get('bpanel4-contact.blacklisted_domains');

        foreach ($blacklistedDomains as $blacklistedDomain) {
            if (str_contains($data['email'], $blacklistedDomain)) {
                throw new MessageIsBlacklistedException();
            }
        }
    }

    private function filterByMessage(array $data): void
    {
        $blacklistedWords = $this->config->get('bpanel4-contact.blacklisted_words');

        foreach ($blacklistedWords as $blacklistedWord) {
            if (str_contains($data['message'], $blacklistedWord)) {
                throw new MessageIsBlacklistedException();
            }
        }
    }

}