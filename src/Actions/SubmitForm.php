<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Actions;

use Bittacora\Bpanel4\Contact\Mail\ContactFormMail;
use Bittacora\Bpanel4\Contact\Models\ContactSubmission;
use Bittacora\Bpanel4\Contact\Services\SpamFilter;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

final class SubmitForm
{
    public function __construct(
        private readonly Mailer $mailer,
        private readonly Repository $config,
        private readonly SpamFilter $spamFilter,
    ) {
    }

    /**
     * @param array<string, string|bool> $data
     */
    public function execute(array $data): void
    {
        $this->spamFilter->filter($data);
        $contactSubmission = new ContactSubmission();
        $contactSubmission->name = $data['name'];
        $contactSubmission->email = $data['email'];
        $contactSubmission->phone = $data['phone'];
        $contactSubmission->message = $data['message'];
        $contactSubmission->acceptance = (bool) $data['acceptance'];
        $contactSubmission->save();

        $mail = new ContactFormMail($data);
        $this->mailer->to($this->config->get('bpanel4.admin_email'))->send($mail);
    }
}
