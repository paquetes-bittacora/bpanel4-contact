<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Http\Livewire;

use Bittacora\Bpanel4\Contact\Models\ContactSubmission;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class SubmissionsDatatable extends DataTableComponent
{
    public ?string $defaultSortColumn = 'created_at';
    public string $defaultSortDirection = 'desc';
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Fecha', 'created_at')->sortable(),
            Column::make('Nombre', 'name')->sortable(),
            Column::make('Email', 'email')->sortable(),
            Column::make('Teléfono', 'phone')->sortable(),
            Column::make('Mensaje', 'message')->sortable(),
            Column::make('Acciones', 'id')->view('bpanel4-contact::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<ContactSubmission>
     */
    public function query(): Builder
    {
        return ContactSubmission::query()
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
                ->orWhere('email', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-contact::bpanel.livewire.submissions-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            ContactSubmission::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
