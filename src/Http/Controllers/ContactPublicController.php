<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Http\Controllers;

use Bittacora\Bpanel4\Contact\Actions\SubmitForm;
use Bittacora\Bpanel4\Contact\Http\Requests\ContactRequest;
use Bittacora\Bpanel4\Contact\Models\ContactInfo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class ContactPublicController
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    public function index(): View
    {
        $contactDetails = ContactInfo::where('id', 1)->firstOrFail();
        return $this->view->make('bpanel4-contact::form', ['contactDetails' => $contactDetails]);
    }

    public function send(ContactRequest $request, SubmitForm $submitForm): RedirectResponse
    {
        $submitForm->execute($request->validated());

        return $this->redirector->route('contact')->with(['alert-success' => 'Gracias por su mensaje, le responderemos lo antes posible']);
    }
}
