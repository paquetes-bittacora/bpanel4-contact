<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Contact\Http\Requests\ContactInfoRequest;
use Bittacora\Bpanel4\Contact\Models\ContactInfo;
use Bittacora\Bpanel4\Contact\Models\ContactSubmission;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class ContactAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('contact-admin.index');
        return $this->view->make('bpanel4-contact::bpanel.index');
    }

    public function show(ContactSubmission $submission): View
    {
        $this->authorize('contact-admin.show');

        return $this->view->make('bpanel4-contact::bpanel.show', ['submission' => $submission]);
    }

    public function destroy(ContactSubmission $submission): RedirectResponse
    {
        $submission->delete();

        return $this->redirector->back()->with(['alert-success' => 'Mensaje eliminado']);
    }

    /**
     * Edición de los datos de contacto de la tienda, no de un mensaje
     */
    public function edit(): View
    {
        $contactInfo = ContactInfo::where('id', 1)->firstOrFail();
        return $this->view->make('bpanel4-contact::bpanel.edit', ['contactInfo' => $contactInfo]);
    }

    public function update(ContactInfoRequest $request): RedirectResponse
    {
        $contactInfo = ContactInfo::where('id', 1)->firstOrFail();

        $contactInfo->introductory_text = $request->validated('introductory_text');
        $contactInfo->contact_details = $request->validated('contact_details');
        $contactInfo->latitude = $request->validated('location.latitude');
        $contactInfo->longitude = $request->validated('location.longitude');
        $contactInfo->zoom = $request->validated('location.zoom');
        $contactInfo->save();

        return $this->redirector->route('contact-admin.edit')->with([
            'alert-success' => 'Datos actualizados',
        ]);
    }
}
