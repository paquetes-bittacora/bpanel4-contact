<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class ContactRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'email' => 'email|required_without:phone',
            'phone' => 'string|required_without:email',
            'message' => 'string|required|max:1000',
            'g-recaptcha-response' => 'required|captcha',
            'acceptance' => 'required',
        ];
    }
}
