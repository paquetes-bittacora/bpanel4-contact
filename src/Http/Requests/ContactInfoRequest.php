<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class ContactInfoRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'introductory_text' => 'string|nullable',
            'contact_details' => 'string|nullable',
            'location.latitude' => 'numeric|nullable',
            'location.longitude' => 'numeric|nullable',
            'location.zoom' => 'numeric|nullable',
        ];
    }
}
