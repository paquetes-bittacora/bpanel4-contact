<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Contact\Models\ContactInfo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-contact:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete de contacto de bPanel4';

    private const PERMISSIONS = ['index', 'show', 'destroy', 'edit', 'update'];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->addEnvConfigEntries();
        $this->giveAdminPermissions();
        $this->registerAdminMenuEntries($adminMenu);
        $this->createContactInfoModel();
    }

    private function addEnvConfigEntries(): void
    {
        $env = App::environment();
        $file = in_array($env, ['production', 'local']) ?
            base_path() . '/.env' :
            base_path() . '/.env.' . $env;

        $fileContents = file_get_contents($file);

        $string = "\n# bPanel4 Contact ------------------------------
NOCAPTCHA_SECRET=
NOCAPTCHA_SITEKEY=
# bPanel4 Contact ------------------------------
";

        if (!str_contains($fileContents, 'bPanel4 Contact')) {
            file_put_contents($file, $string, FILE_APPEND);
        }
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'contact-admin.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function registerAdminMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('configuration', 'Configuración', 'fa fa-cog');
        $adminMenu->createModule(
            'configuration',
            'contact-admin',
            'Contacto',
            'far fa-envelope'
        );
        $adminMenu->createAction(
            'contact-admin',
            'Editar inf. contacto',
            'edit',
            'fas fa-pencil'
        );
        $adminMenu->createAction(
            'contact-admin',
            'Mensajes recibidos',
            'index',
            'fas fa-list'
        );
    }

    private function createContactInfoModel(): void
    {
        $contactInfo = ContactInfo::firstOrCreate(
            ['id' => 1],
            [
                'introductory_text' => '',
                'contact_details' => '',
                'latitude' => 38.87789,
                'longitude' => -6.97061,
                'zoom' => 12,
            ]
        );
    }
}
