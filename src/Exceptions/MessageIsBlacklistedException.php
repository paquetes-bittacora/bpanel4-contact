<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Exceptions;

final class MessageIsBlacklistedException extends \Exception
{
    protected $message = 'El dominio o texto del mensaje está en la lista negra';
}