<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Tests\Acceptance;

use Anhskohbo\NoCaptcha\Facades\NoCaptcha;
use Bittacora\Bpanel4\Contact\Mail\ContactFormMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

final class ContactFormTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->withoutExceptionHandling();
        NoCaptcha::shouldReceive('verifyResponse')->zeroOrMoreTimes()->andReturn(true);
        Config::set('bpanel4.admin_email', 'admin@email.com');
    }

    public function testElFormularioSeEnviaYSeGuardaEnLaBd(): void
    {
        $email = $this->faker->email();

        $response = $this->sendValidForm($email);

        $response->assertValid();
        $this->assertDatabaseHas('contact_submissions', ['email' => $email]);
    }

    public function testEnviaElEmailAlAdministrador(): void
    {
        $mail = Mail::fake();
        $email = $this->faker->email();

        $response = $this->sendValidForm($email);

        $response->assertValid();

        $mail->assertSent(ContactFormMail::class, function (ContactFormMail $mail): bool {
            self::assertEquals('admin@email.com', $mail->to[0]['address']);
            return true;
        });
    }

    private function sendValidForm(string $email): \Illuminate\Testing\TestResponse
    {
        return $this->post($this->urlGenerator->route('send-contact'), [
            'name' => $this->faker->name(),
            'email' => $email,
            'phone' => $this->faker->phoneNumber(),
            'message' => $this->faker->text(),
            'g-recaptcha-response' => 1,
            'acceptance' => 1,
        ]);
    }
}
