<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Contact\Tests\Acceptance;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    public function testElComandoDeInstalacionSeEjecuta(): void
    {
        touch(base_path() . '/.env.testing');
        $result = $this->artisan('bpanel4-contact:install');
        $result->assertOk();
    }
}
